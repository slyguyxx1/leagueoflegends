﻿using Xamarin.Forms;
using LeagueApp.Champions;
using System.Collections.ObjectModel;
using System.Collections.Generic;





namespace LeagueApp
{
    public partial class LeagueAppPage : ContentPage
    {
        
        int attacker = 0; //to see which attacker object to make
        int defender = 0; //to see which defender object to make

        Vayne vayne = new Vayne();
        Caitlyn caitlyn = new Caitlyn();
        Darius darius = new Darius();
        Rek rek = new Rek();
        ChoGath cho = new ChoGath();
        KogMaw kog = new KogMaw();

        string empty = "";

        
        public LeagueAppPage()
        {
            
            InitializeComponent();

            populateFeastPicker();
        }

        ObservableCollection<int> feastCollection = new ObservableCollection<int>();

        void Vayne_Clicked(object sender, System.EventArgs e)
        {
            
            attacker = 1;
            caitlynButton.BackgroundColor = Xamarin.Forms.Color.Transparent;
            kogButton.BackgroundColor = Xamarin.Forms.Color.Transparent;
            vayneButton.BackgroundColor = Xamarin.Forms.Color.White;


            adc_Selected.TextColor = Xamarin.Forms.Color.Green;
            adc_Selected.Text = "Vayne Selected";
        }

        void Caitlyn_Clicked(object sender, System.EventArgs e)
        {
            attacker = 2;
            vayneButton.BackgroundColor = Xamarin.Forms.Color.Transparent;
            kogButton.BackgroundColor = Xamarin.Forms.Color.Transparent;
            caitlynButton.BackgroundColor = Xamarin.Forms.Color.White;
            adc_Selected.TextColor = Xamarin.Forms.Color.Green;
            adc_Selected.Text = "Caitlyn Selected";
        }

        void Kog_Clicked(object sender, System.EventArgs e)
        {
            attacker = 3;
            vayneButton.BackgroundColor = Xamarin.Forms.Color.Transparent;
            caitlynButton.BackgroundColor = Xamarin.Forms.Color.Transparent;
            kogButton.BackgroundColor = Xamarin.Forms.Color.White;
            adc_Selected.TextColor = Xamarin.Forms.Color.Green;
            adc_Selected.Text = "Kog'Maw Selected";
        }

        void Darius_Clicked(object sender, System.EventArgs e)
        {
            howManyFeastStacks.IsVisible = false;
            feastPicker.IsVisible = false;
            defender = 1;
            rekButton.BackgroundColor = Xamarin.Forms.Color.Transparent;
            choButton.BackgroundColor = Xamarin.Forms.Color.Transparent;
            dariusButton.BackgroundColor = Xamarin.Forms.Color.White;
            tank_Selected.TextColor = Xamarin.Forms.Color.Firebrick;
            tank_Selected.Text = "Darius Selected";
        }

        void Reksai_Clicked(object sender, System.EventArgs e)
        {
            howManyFeastStacks.IsVisible = false;
            feastPicker.IsVisible = false;
            defender = 2;
            dariusButton.BackgroundColor = Xamarin.Forms.Color.Transparent;
            choButton.BackgroundColor = Xamarin.Forms.Color.Transparent;
            rekButton.BackgroundColor = Xamarin.Forms.Color.White;
            tank_Selected.TextColor = Xamarin.Forms.Color.Firebrick;
            tank_Selected.Text = "Rek'sai Selected";
        }

        void Cho_Clicked(object sender, System.EventArgs e)
        {
            howManyFeastStacks.IsVisible = true;
            feastPicker.IsVisible = true;
            defender = 3;
            dariusButton.BackgroundColor = Xamarin.Forms.Color.Transparent;
            rekButton.BackgroundColor = Xamarin.Forms.Color.Transparent;
            choButton.BackgroundColor = Xamarin.Forms.Color.White;
            tank_Selected.TextColor = Xamarin.Forms.Color.Firebrick;
            tank_Selected.Text = "Chogath Selected";
        }


        ///Calculate clicked/////////////////////////////////////////////////////

        void Calculate_Clicked(object sender, System.EventArgs e)
        {
            
            detailsButton.Text = "more info";
            if(attacker == 0)
            {
                adc_Selected.TextColor = Xamarin.Forms.Color.Red;
                adc_Selected.Text = "You have not selected an ADC!";

            }
            if(defender == 0)
            {
                tank_Selected.TextColor = Xamarin.Forms.Color.Red;
                tank_Selected.Text = "you have not selected a TANK!";
            }


            //didnt do it in attackers class / defender class so this part is just alot of ifs, in retrospect would have done it the other way (time constraint)

            //vayne darius 
            if(attacker ==1 && defender ==1)
            {
                empty = "";


                double referenceOfHealth = darius.hp;
                Label tank = new Label();
                tank.Text = "Darius";
                tank.TextColor = Xamarin.Forms.Color.Red;

                Label adc = new Label();
                adc.Text = "Vayne";
                tank.TextColor = Xamarin.Forms.Color.Green;

                int numAttacks =vayne.Calculate(vayne,darius,ref empty);

                double howLongAttacking = numAttacks / vayne.atkspeed;

                Label stringOfNumAttacks = new Label();
                stringOfNumAttacks.Text = numAttacks.ToString();
                if(darius.hp < 0)
                {
                    darius.hp = referenceOfHealth; 
                }
                stringOfNumAttacks.TextColor = Xamarin.Forms.Color.Gold;

                var formated = new FormattedString();
                formated.Spans.Add(new Span { Text = "It took " });
                formated.Spans.Add(new Span { Text = adc.Text, ForegroundColor = Xamarin.Forms.Color.Green });
                formated.Spans.Add(new Span { Text = " " });
                formated.Spans.Add(new Span { Text = stringOfNumAttacks.Text, ForegroundColor = Xamarin.Forms.Color.Gold });
                formated.Spans.Add(new Span { Text = " auto attacks to defeat " });
                formated.Spans.Add(new Span { Text = tank.Text, ForegroundColor = Xamarin.Forms.Color.Firebrick });
                formated.Spans.Add(new Span { Text = ". " });


                calculateOutput.FormattedText = formated;

                var asFormat = new FormattedString();
                asFormat.Spans.Add(new Span { Text = "Which would take " });
                asFormat.Spans.Add(new Span { Text = System.Math.Round(howLongAttacking, 2).ToString(), ForegroundColor = Xamarin.Forms.Color.AliceBlue });
                asFormat.Spans.Add(new Span { Text = " Seconds." });

                asOutput.FormattedText = asFormat;

            }


            //vayne reksai
            if(attacker == 1 && defender == 2)
            {
                empty = "";

                double referenceOfHealth = rek.hp;
                Label tank = new Label();
                tank.Text = "Reksai";
                tank.TextColor = Xamarin.Forms.Color.Red;

                Label adc = new Label();
                adc.Text = "Vayne";
                tank.TextColor = Xamarin.Forms.Color.Green;

                int numAttacks = vayne.Calculate2(vayne, rek,ref empty);
                double howLongAttacking = numAttacks / vayne.atkspeed;
                if(rek.hp < 0)
                {
                    rek.hp = referenceOfHealth; // IMPORTANT during calculation hp attribute is reduced. So when doing items update the hp, this line fixes the issue of attacking when the defender is at 0, or will print 0 as "num attacks"
                }
                Label stringOfNumAttacks = new Label();
                stringOfNumAttacks.Text = numAttacks.ToString();
                stringOfNumAttacks.TextColor = Xamarin.Forms.Color.Gold;

                var formated = new FormattedString();
                formated.Spans.Add(new Span { Text = "It took " });
                formated.Spans.Add(new Span { Text = adc.Text, ForegroundColor = Xamarin.Forms.Color.Green });
                formated.Spans.Add(new Span { Text = " " });
                formated.Spans.Add(new Span { Text = stringOfNumAttacks.Text, ForegroundColor = Xamarin.Forms.Color.Gold });
                formated.Spans.Add(new Span { Text = " auto attacks to defeat " });
                formated.Spans.Add(new Span { Text = tank.Text, ForegroundColor = Xamarin.Forms.Color.Firebrick });
                formated.Spans.Add(new Span { Text = "." });


                calculateOutput.FormattedText = formated;

                var asFormat = new FormattedString();
                asFormat.Spans.Add(new Span { Text = "Which would take " });
                asFormat.Spans.Add(new Span { Text = System.Math.Round(howLongAttacking, 2).ToString(), ForegroundColor = Xamarin.Forms.Color.AliceBlue });
                asFormat.Spans.Add(new Span { Text = " Seconds." });

                asOutput.FormattedText = asFormat;

            }

            //vayne cho
            if (attacker == 1 && defender == 3)
            {
                empty = "";


                double referenceOfHealth = cho.hp;
                Label tank = new Label();
                tank.Text = "Cho'Gath";
                tank.TextColor = Xamarin.Forms.Color.Red;

                Label adc = new Label();
                adc.Text = "Vayne";
                tank.TextColor = Xamarin.Forms.Color.Green;

                int numAttacks = vayne.Calculate3(vayne, cho, ref empty);
                double howLongAttacking = numAttacks / vayne.atkspeed;
                if (cho.hp < 0)
                {
                    cho.hp = referenceOfHealth; // IMPORTANT during calculation hp attribute is reduced. So when doing items update the hp, this line fixes the issue of attacking when the defender is at 0, or will print 0 as "num attacks"
                }
                Label stringOfNumAttacks = new Label();
                stringOfNumAttacks.Text = numAttacks.ToString();
                stringOfNumAttacks.TextColor = Xamarin.Forms.Color.Gold;

                var formated = new FormattedString();
                formated.Spans.Add(new Span { Text = "It took " });
                formated.Spans.Add(new Span { Text = adc.Text, ForegroundColor = Xamarin.Forms.Color.Green });
                formated.Spans.Add(new Span { Text = " " });
                formated.Spans.Add(new Span { Text = stringOfNumAttacks.Text, ForegroundColor = Xamarin.Forms.Color.Gold });
                formated.Spans.Add(new Span { Text = " auto attacks to defeat " });
                formated.Spans.Add(new Span { Text = tank.Text, ForegroundColor = Xamarin.Forms.Color.Firebrick });
                formated.Spans.Add(new Span { Text = "." });

                calculateOutput.FormattedText = formated;

                var asFormat = new FormattedString();
                asFormat.Spans.Add(new Span { Text = "Which would take " });
                asFormat.Spans.Add(new Span { Text = System.Math.Round(howLongAttacking, 2).ToString(), ForegroundColor = Xamarin.Forms.Color.AliceBlue });
                asFormat.Spans.Add(new Span { Text = "." });

                asOutput.FormattedText = asFormat;

            }

            //caitlyn darius
            if (attacker == 2 && defender == 1)
            {
                caitlyn.headshot_marker = 0;
                empty = "";


                double referenceOfHealth = darius.hp;
                Label tank = new Label();
                tank.Text = "Darius";
                tank.TextColor = Xamarin.Forms.Color.Red;

                Label adc = new Label();
                adc.Text = "Caitlyn";
                tank.TextColor = Xamarin.Forms.Color.Green;

                int numAttacks = 0;
                if (darius.hp < 0)
                {
                    darius.hp = referenceOfHealth; // IMPORTANT during calculation hp attribute is reduced. So when doing items update the hp, this line fixes the issue of attacking when the defender is at 0, or will print 0 as "num attacks"
                    numAttacks = caitlyn.Calculate1(caitlyn, darius,ref empty);
                }
                else
                    numAttacks = caitlyn.Calculate1(caitlyn, darius,ref empty);

                double howLongAttacking = numAttacks / caitlyn.atkspeed;


              

                Label stringOfNumAttacks = new Label();
                stringOfNumAttacks.Text = numAttacks.ToString();
                stringOfNumAttacks.TextColor = Xamarin.Forms.Color.Gold;

                var formated = new FormattedString();
                formated.Spans.Add(new Span { Text = "It took " });
                formated.Spans.Add(new Span { Text = adc.Text, ForegroundColor = Xamarin.Forms.Color.Green });
                formated.Spans.Add(new Span { Text = " " });
                formated.Spans.Add(new Span { Text = stringOfNumAttacks.Text, ForegroundColor = Xamarin.Forms.Color.Gold });
                formated.Spans.Add(new Span { Text = " auto attacks to defeat " });
                formated.Spans.Add(new Span { Text = tank.Text, ForegroundColor = Xamarin.Forms.Color.Firebrick });
                formated.Spans.Add(new Span { Text = "." });


                calculateOutput.FormattedText = formated;

                var asFormat = new FormattedString();
                asFormat.Spans.Add(new Span { Text = "Which would take " });
                asFormat.Spans.Add(new Span { Text = System.Math.Round(howLongAttacking, 2).ToString(), ForegroundColor = Xamarin.Forms.Color.AliceBlue });
                asFormat.Spans.Add(new Span { Text = " Seconds." });

                asOutput.FormattedText = asFormat;

            }

            //caitlyn rek
            if (attacker == 2 && defender == 2)
            {
                caitlyn.headshot_marker = 0;
                empty = "";


                double referenceOfHealth = rek.max_health;
                Label tank = new Label();
                tank.Text = "Reksai";
                tank.TextColor = Xamarin.Forms.Color.Red;

                Label adc = new Label();
                adc.Text = "Caitlyn";
                tank.TextColor = Xamarin.Forms.Color.Green;

                int numAttacks = 0;
                if (rek.hp < 0)
                {
                    rek.hp = referenceOfHealth; // IMPORTANT during calculation hp attribute is reduced. So when doing items update the hp, this line fixes the issue of attacking when the defender is at 0, or will print 0 as "num attacks"
                    numAttacks = caitlyn.Calculate2(caitlyn, rek,ref empty);
                }
                else
                    numAttacks = caitlyn.Calculate2(caitlyn, rek,ref empty);

                double howLongAttacking = numAttacks / caitlyn.atkspeed;
                
                Label stringOfNumAttacks = new Label();
                stringOfNumAttacks.Text = numAttacks.ToString();
                stringOfNumAttacks.TextColor = Xamarin.Forms.Color.Gold;

                var formated = new FormattedString();
                formated.Spans.Add(new Span { Text = "It took " });
                formated.Spans.Add(new Span { Text = adc.Text, ForegroundColor = Xamarin.Forms.Color.Green });
                formated.Spans.Add(new Span { Text = " " });
                formated.Spans.Add(new Span { Text = stringOfNumAttacks.Text, ForegroundColor = Xamarin.Forms.Color.Gold });
                formated.Spans.Add(new Span { Text = " auto attacks to defeat " });
                formated.Spans.Add(new Span { Text = tank.Text, ForegroundColor = Xamarin.Forms.Color.Firebrick});
                formated.Spans.Add(new Span { Text = "." });


                calculateOutput.FormattedText = formated;

                var asFormat = new FormattedString();
                asFormat.Spans.Add(new Span { Text = "Which would take " });
                asFormat.Spans.Add(new Span { Text = System.Math.Round(howLongAttacking, 2).ToString(), ForegroundColor = Xamarin.Forms.Color.AliceBlue });
                asFormat.Spans.Add(new Span { Text = " Seconds." });

                asOutput.FormattedText = asFormat;

            }


            //caitlyn cho
            if (attacker == 2 && defender == 3)
            {
                caitlyn.headshot_marker = 0;
                empty = "";


                double startHp = cho.hp;
                Label tank = new Label();
                tank.Text = "Cho'Gath";
                tank.TextColor = Xamarin.Forms.Color.Red;

                Label adc = new Label();
                adc.Text = "Caitlyn";
                tank.TextColor = Xamarin.Forms.Color.Green;

                int numAttacks = 0;
                if (cho.hp < 0)
                {
                    cho.hp = startHp; // IMPORTANT during calculation hp attribute is reduced. So when doing items update the hp, this line fixes the issue of attacking when the defender is at 0, or will print 0 as "num attacks"
                    numAttacks = caitlyn.Calculate3(caitlyn, cho,ref empty);
                }
                else
                    numAttacks = caitlyn.Calculate3(caitlyn, cho,ref empty);

                double howLongAttacking = numAttacks / caitlyn.atkspeed;
                
                Label stringOfNumAttacks = new Label();
                stringOfNumAttacks.Text = numAttacks.ToString();
                stringOfNumAttacks.TextColor = Xamarin.Forms.Color.Gold;

                var formated = new FormattedString();
                formated.Spans.Add(new Span { Text = "It took " });
                formated.Spans.Add(new Span { Text = adc.Text, ForegroundColor = Xamarin.Forms.Color.Green });
                formated.Spans.Add(new Span { Text = " " });
                formated.Spans.Add(new Span { Text = stringOfNumAttacks.Text, ForegroundColor = Xamarin.Forms.Color.Gold });
                formated.Spans.Add(new Span { Text = " auto attacks to defeat " });
                formated.Spans.Add(new Span { Text = tank.Text, ForegroundColor = Xamarin.Forms.Color.Firebrick });
                formated.Spans.Add(new Span { Text = "." });


                calculateOutput.FormattedText = formated;

                var asFormat = new FormattedString();
                asFormat.Spans.Add(new Span { Text = "Which would take " });
                asFormat.Spans.Add(new Span { Text = System.Math.Round(howLongAttacking, 2).ToString(), ForegroundColor = Xamarin.Forms.Color.AliceBlue });
                asFormat.Spans.Add(new Span { Text = " Seconds." });

                asOutput.FormattedText = asFormat;

            }

            ////kog darius
            if (attacker == 3 && defender == 1)
            {
                empty = "";


                double referenceOfHealth = darius.hp;
                Label tank = new Label();
                tank.Text = "Darius";
                tank.TextColor = Xamarin.Forms.Color.Red;

                Label adc = new Label();
                adc.Text = "Kog'Maw";
                tank.TextColor = Xamarin.Forms.Color.Green;

                int numAttacks = kog.Calculate(kog, darius,ref empty);

                double howLongAttacking = numAttacks / kog.atkspeed;
                if (darius.hp < 0)
                {
                    darius.hp = referenceOfHealth; // IMPORTANT during calculation hp attribute is reduced. So when doing items update the hp, this line fixes the issue of attacking when the defender is at 0, or will print 0 as "num attacks"
                }
                Label stringOfNumAttacks = new Label();
                stringOfNumAttacks.Text = numAttacks.ToString();
                stringOfNumAttacks.TextColor = Xamarin.Forms.Color.Gold;

                var formated = new FormattedString();
                formated.Spans.Add(new Span { Text = "It took " });
                formated.Spans.Add(new Span { Text = adc.Text, ForegroundColor = Xamarin.Forms.Color.Green });
                formated.Spans.Add(new Span { Text = " " });
                formated.Spans.Add(new Span { Text = stringOfNumAttacks.Text, ForegroundColor = Xamarin.Forms.Color.Gold });
                formated.Spans.Add(new Span { Text = " auto attacks to defeat " });
                formated.Spans.Add(new Span { Text = tank.Text, ForegroundColor = Xamarin.Forms.Color.Firebrick });
                formated.Spans.Add(new Span { Text = "." });


                calculateOutput.FormattedText = formated;

                var asFormat = new FormattedString();
                asFormat.Spans.Add(new Span { Text = "Which would take " });
                asFormat.Spans.Add(new Span { Text = System.Math.Round(howLongAttacking, 2).ToString(), ForegroundColor = Xamarin.Forms.Color.AliceBlue });
                asFormat.Spans.Add(new Span { Text = " Seconds." });

                asOutput.FormattedText = asFormat;

            }

            //kog reksai
            if(attacker ==3 && defender == 2)
            {
                empty = "";


                double referenceOfHealth = rek.hp;
                Label tank = new Label();
                tank.Text = "Reksai";
                tank.TextColor = Xamarin.Forms.Color.Red;

                Label adc = new Label();
                adc.Text = "Kog'Maw";
                tank.TextColor = Xamarin.Forms.Color.Green;

                int numAttacks = kog.Calculate2(kog, rek,ref empty);
                double howLongAttacking = numAttacks / kog.atkspeed;
                if (rek.hp < 0)
                {
                    rek.hp = referenceOfHealth; // IMPORTANT during calculation hp attribute is reduced. So when doing items update the hp, this line fixes the issue of attacking when the defender is at 0, or will print 0 as "num attacks"
                }
                Label stringOfNumAttacks = new Label();
                stringOfNumAttacks.Text = numAttacks.ToString();
                stringOfNumAttacks.TextColor = Xamarin.Forms.Color.Gold;

                var formated = new FormattedString();
                formated.Spans.Add(new Span { Text = "It took " });
                formated.Spans.Add(new Span { Text = adc.Text, ForegroundColor = Xamarin.Forms.Color.Green });
                formated.Spans.Add(new Span { Text = " " });
                formated.Spans.Add(new Span { Text = stringOfNumAttacks.Text, ForegroundColor = Xamarin.Forms.Color.Gold });
                formated.Spans.Add(new Span { Text = " auto attacks to defeat " });
                formated.Spans.Add(new Span { Text = tank.Text, ForegroundColor = Xamarin.Forms.Color.Firebrick });
                formated.Spans.Add(new Span { Text = "." });


                calculateOutput.FormattedText = formated;

                var asFormat = new FormattedString();
                asFormat.Spans.Add(new Span { Text = "Which would take " });
                asFormat.Spans.Add(new Span { Text = System.Math.Round(howLongAttacking, 2).ToString(), ForegroundColor = Xamarin.Forms.Color.AliceBlue });
                asFormat.Spans.Add(new Span { Text = " Seconds." });


                asOutput.FormattedText = asFormat;


            }

            //kog cho
            if(attacker ==3 && defender == 3)
            {
                empty = "";


                double referenceOfHealth = cho.hp;
                Label tank = new Label();
                tank.Text = "Cho'Gath";
                tank.TextColor = Xamarin.Forms.Color.Red;

                Label adc = new Label();
                adc.Text = "Kog'Maw";
                tank.TextColor = Xamarin.Forms.Color.Green;

                int numAttacks = kog.Calculate3(kog, cho,ref empty);
                double howLongAttacking = numAttacks / kog.atkspeed;




                if (cho.hp < 0)
                {
                    cho.hp = referenceOfHealth; // IMPORTANT during calculation hp attribute is reduced. So when doing items update the hp, this line fixes the issue of attacking when the defender is at 0, or will print 0 as "num attacks"
                }
                Label stringOfNumAttacks = new Label();
                stringOfNumAttacks.Text = numAttacks.ToString();
                stringOfNumAttacks.TextColor = Xamarin.Forms.Color.Gold;

                var formated = new FormattedString();
                formated.Spans.Add(new Span { Text = "It took " });
                formated.Spans.Add(new Span { Text = adc.Text, ForegroundColor = Xamarin.Forms.Color.Green });
                formated.Spans.Add(new Span { Text = " " });
                formated.Spans.Add(new Span { Text = stringOfNumAttacks.Text, ForegroundColor = Xamarin.Forms.Color.Gold });
                formated.Spans.Add(new Span { Text = " auto attacks to defeat " });
                formated.Spans.Add(new Span { Text = tank.Text, ForegroundColor = Xamarin.Forms.Color.Firebrick });
                formated.Spans.Add(new Span { Text = "." });


                calculateOutput.FormattedText = formated;

                var asFormat = new FormattedString();
                asFormat.Spans.Add(new Span { Text = "Which would take " });
                asFormat.Spans.Add(new Span { Text = System.Math.Round(howLongAttacking, 2).ToString(), ForegroundColor = Xamarin.Forms.Color.AliceBlue });
                asFormat.Spans.Add(new Span { Text = " Seconds." });

                asOutput.FormattedText = asFormat;

            }
            
        }



        public void populateFeastPicker()
        {
            /*
            for (int i = 0; i <= 18; i++)
            {
                int current = i;
                feastCollection.Add(current);
            }
            */


            Dictionary<string, int> intToAdd = new Dictionary<string, int>()
            {
                {"0",0},
                {"1",1},
                {"2",2},
                {"3",3},
                {"4",4},
                {"5",5},
                {"6",6},
                {"7",7},
                {"8",8},
                {"9",9},
            };

            foreach(var item in intToAdd)
            {
                feastPicker.Items.Add(item.Key);
            }
            feastPicker.SelectedIndex = 0;


        }

        void details_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new moreInfoPage(empty));
        }

        void Feast_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if(feastPicker.SelectedIndex == -1)
            {
                
            }
            else
            {

                int indexOfPicker = feastPicker.SelectedIndex;
                if(indexOfPicker == 0)
                {
                    cho.hp = cho.max_health;
                }
                else if(indexOfPicker == 1)
                {
                    cho.hp += (160 * 1);
                }
                else if (indexOfPicker == 2)
                {
                    cho.hp += (160 * 2);
                }
                else if (indexOfPicker == 3)
                {
                    cho.hp += (160 * 3);
                }
                else if (indexOfPicker == 4)
                {
                    cho.hp += (160 * 4);
                }
                else if (indexOfPicker == 5)
                {
                    cho.hp += (160 * 5);
                }
                else if (indexOfPicker == 6)
                {
                    cho.hp += (160 * 6);
                }
                else if (indexOfPicker == 7)
                {
                    cho.hp += (160 * 7);
                }
                else if (indexOfPicker == 8)
                {
                    cho.hp += (160 * 8);
                }
                else if (indexOfPicker == 9)
                {
                    cho.hp += (160 * 9);
                }





            }

        }
    }
}
