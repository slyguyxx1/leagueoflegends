﻿using System;

using Xamarin.Forms;

namespace LeagueApp
{
    public class SetupPage : ContentPage
    {
        public SetupPage()
        {
            Content = new StackLayout
            {
                Children = {
                    new Label { Text = "Hello ContentPage" }
                }
            };
        }
    }
}

