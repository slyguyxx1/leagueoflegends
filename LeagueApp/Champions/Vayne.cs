﻿using System;
namespace LeagueApp.Champions
{
    public class Vayne
    {
        public  double initial_ad = 92.2;
        public  double initial_as = 1.027138;
        public  int crit_chance_marker = 60;
        public  int silver_bolts_marker = 0;
        public  int crit_chance = 0;

        public double atkspeed = 1.027138;
        public double ad = 92.2;

        public string emptyHold = "";


        public int crit_maker()
        {
            Random random = new Random();

            int xRan = random.Next(1, 100);

            return xRan;
        }



        public double Attack(ref Vayne attacker, Darius defender,ref string attckHold)
        {

            int xRan = crit_maker();

            double silver_bolts_output;



            double damage = attacker.ad;  //setting base attack damage
                                          //damage=item.getItems(damage);
            double damage_dealt;


            if (xRan < attacker.crit_chance) //if its a crit
            {
                attacker.silver_bolts_marker++;



                damage_dealt = (damage * 2) * defender.armor_Reduction();
                if (attacker.silver_bolts_marker == 3)
                {
                    attacker.silver_bolts_marker = 0;
                    damage_dealt += defender.max_health * .12;

                    attckHold += "you did true damage and crit: \n" + damage_dealt;
                    attckHold += "HP left: " + defender.hp + "\n\n";

                    return damage_dealt;

                }
                else
                    

                return damage_dealt;
            }
            else  //its not a crit
            {
                attacker.silver_bolts_marker++;

       
                damage_dealt = damage * defender.armor_Reduction();
                if (attacker.silver_bolts_marker == 3)
                {
                    double trueDealt = 0;
                    double basicDmg = damage_dealt;
                    attacker.silver_bolts_marker = 0;
                    damage_dealt += defender.max_health * .12;

                    trueDealt = defender.max_health * .12;

                    silver_bolts_output = defender.max_health * .12;

                    attckHold += "SILVERBOLTS PROC! ( " + trueDealt + " ) \n";

                    attckHold += "Basic attack did " + basicDmg + "\n\n";

                    //attckHold += "HP left: " + defender.hp + "\n\n";

                    return damage_dealt;

                   
                }
                else
                {   attckHold += "Basic Attack: " + damage_dealt + "\n\n";
                    
                }
                    

                return damage_dealt;
            }
        }


        public double Attack2(ref Vayne attacker, Rek defender,ref string attckHold)
        {

            int xRan = crit_maker();

            double silver_bolts_output;



            double damage = attacker.ad;  //setting base attack damage
                                          //damage=item.getItems(damage);
            double damage_dealt;

            if (xRan < attacker.crit_chance) //if its a crit
            {
                attacker.silver_bolts_marker++;



                damage_dealt = (damage * 2) * defender.armor_Reduction();
                if (attacker.silver_bolts_marker == 3)
                {
                    attacker.silver_bolts_marker = 0;
                    damage_dealt += defender.max_health * .12;

                    attckHold += "you did true damage and crit: \n" + damage_dealt;


                    return damage_dealt;
                }
                else
                {
                    
                    return damage_dealt;
                }
                    


            }
            else  //its not a crit
            {
                attacker.silver_bolts_marker++;

                //cout << attacker.silver_bolts_marker << endl;

                damage_dealt = damage * defender.armor_Reduction();
                if (attacker.silver_bolts_marker == 3)
                {
                    double trueDealt = 0;
                    double basicDmg = damage_dealt;
                    attacker.silver_bolts_marker = 0;
                    damage_dealt += defender.max_health * .12;
                    trueDealt += defender.max_health * .12;

                    silver_bolts_output = defender.max_health * .12;

                    attckHold += "SILVERBOLTS PROC! ( " + trueDealt + " ) \n";

                    attckHold += "Basic attack did " + basicDmg + "\n\n";

                 
                    return damage_dealt;
                }
                else
                {
                    attckHold += "Basic Attack: " + damage_dealt + "\n\n";

                }

                return damage_dealt;

            }

        }

        public double Attack3(ref Vayne attacker, ChoGath defender,ref string attckHold)
        {

            int xRan = crit_maker();

            double silver_bolts_output;



            double damage = attacker.ad;  //setting base attack damage
                                          //damage=item.getItems(damage);
            double damage_dealt;

  
            if (xRan < attacker.crit_chance) //if its a crit
            {
                attacker.silver_bolts_marker++;



                damage_dealt = (damage * 2) * defender.armor_Reduction();
                if (attacker.silver_bolts_marker == 3)
                {
                    attacker.silver_bolts_marker = 0;
                    damage_dealt += defender.max_health * .12;
                    attckHold += "you did true damage and crit: \n" + damage_dealt;
                    return damage_dealt;
                }
                else
                {
                    
                    return damage_dealt;
                }



            }
            else  //its not a crit
            {
                attacker.silver_bolts_marker++;


                damage_dealt = damage * defender.armor_Reduction();
                if (attacker.silver_bolts_marker == 3)
                {
                    double trueDealt = 0;
                    double basicDmg = damage_dealt;
                    attacker.silver_bolts_marker = 0;
                    damage_dealt += defender.max_health * .12;
                    trueDealt += defender.max_health * .12;

                    silver_bolts_output = defender.max_health * .12;

                    attckHold += "SILVERBOLTS PROC! ( " + trueDealt + " ) \n";

                    attckHold += "Basic attack did " + basicDmg + "\n\n";


                    return damage_dealt;
                }
                else
                    attckHold += "Basic Attack: " + damage_dealt + "\n\n";

                
                return damage_dealt;
            }

        }

        public int Calculate(Vayne attacker, Darius defender,ref string holder)
        {
            
            //set the attack count
            int attack_count = 0;

            holder += "STARTING HP: " + defender.hp + "\n";
            //if defender is living keep attacking
            while (defender.hp > 0)
            {



                //up the count, as we can hit living champion
                attack_count++;

                //the new hp is done after the dmg done
                defender.hp -= attacker.Attack(ref attacker, defender,ref holder);

                holder += "HP REMAINING: " + defender.hp + "\n";

                emptyHold = holder;

            } //end of while loop

            defender.hp = defender.max_health; //setting health back to ful after calculations
            return attack_count;

           
        }




        public int Calculate2(Vayne attacker, Rek defender,ref string holder)
        {
            //set the attack count
            int attack_count = 0;

            holder += "STARTING HP: " + defender.hp + "\n";

            //if defender is living keep attacking
            while (defender.hp > 0)
            {



                //up the count, as we can hit living champion
                attack_count++;

                //the new hp is done after the dmg done
                defender.hp -= attacker.Attack2(ref attacker, defender,ref holder);

                holder += "HP REMAINING: " + defender.hp + "\n";


            } //end of while loop
            defender.hp = defender.max_health; //setting health back to ful after calculations
            return attack_count;


        }

        public int Calculate3(Vayne attacker, ChoGath defender,ref string holder)
        {
            //set the attack count
            int attack_count = 0;

            holder += "STARTING HP: " + defender.hp + "\n";

            //if defender is living keep attacking
            while (defender.hp > 0)
            {



                //up the count, as we can hit living champion
                attack_count++;

                //the new hp is done after the dmg done
                defender.hp -= attacker.Attack3(ref attacker, defender,ref holder);

                holder += "HP REMAINING: " + defender.hp + "\n";


            } //end of while loop
            defender.hp = defender.max_health; //setting health back to ful after calculations
            return attack_count;


        }








    }
}
