﻿using System;
namespace LeagueApp.Champions
{
    public class ChoGath
    {
        public double max_health = 2015;
        //2015
        public double hp = 2015;
        public double armor = 96.8;
        public double mr = 53.4;

        public double get_effective()
        {
            double eHealth = (1 + (armor / 100)) * hp;

            return eHealth;
        }

        public double armor_Reduction()
        {
            if (armor >= 0)
            {
                return 100 / (100 + armor);
            }
            else
                return 2 - (100 / (100 - armor));
        }

        public double magic_Reduction()
        {
            if (mr >= 0)
            {
                return 100 / (100 + mr);
            }
            else
                return 2 - (100 / (100 - mr));
        }


    }
}
